﻿using Unity.Entities;

namespace Belerion.Games.ThreeInRowUnity.Spawning
{
    [InternalBufferCapacity(8)]
    public struct PrefabsBufferElement : IBufferElementData
    {
        public Entity Prefab;
    }
}
