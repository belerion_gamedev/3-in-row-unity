﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Entities;
using UnityEngine;

namespace Belerion.Games.ThreeInRowUnity.Spawning
{
    public abstract class AbstractSpawnerAuthoring<TPrefabKey, TSpawnerComponent> : MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity
        where TPrefabKey : Enum
        where TSpawnerComponent : struct, IComponentData
    {
        protected Dictionary<TPrefabKey, GameObject> PrefabsMap = new Dictionary<TPrefabKey, GameObject>();

        public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
        {
            referencedPrefabs.AddRange(PrefabsMap.Select(keyValue => keyValue.Value));
        }

        public void Convert(Entity entity, EntityManager destinationManager, GameObjectConversionSystem conversionSystem)
        {
            destinationManager.AddComponent<TSpawnerComponent>(entity);
            var buffer = destinationManager.AddBuffer<PrefabsBufferElement>(entity);

            for (var i = 0; i <= PrefabsMap.Max(prefab => (int)(object)prefab.Key); i++)
            {
                PrefabsMap.TryGetValue((TPrefabKey)(object)i, out var prefab);
                buffer.Add(new PrefabsBufferElement
                {
                    Prefab = prefab == null ? Entity.Null : conversionSystem.GetPrimaryEntity(prefab)
                });
            }
        }
    }
}