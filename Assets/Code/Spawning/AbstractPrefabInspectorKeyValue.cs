﻿using System;
using UnityEngine;

namespace Belerion.Games.ThreeInRowUnity.Spawning
{
    public abstract class AbstractPrefabInspectorKeyValue<TKey>
        where TKey : Enum
    {
        public TKey Key;
        public GameObject Prefab;
    }
}
