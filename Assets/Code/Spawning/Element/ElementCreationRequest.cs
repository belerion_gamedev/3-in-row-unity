﻿using Unity.Entities;

namespace Belerion.Games.ThreeInRowUnity.Spawning.Element
{
    public struct ElementCreationRequest : IComponentData
    {
        public ElementTypes ElementType;
    }
}
