﻿using System.Linq;
using Unity.Collections;
using Unity.Entities;

namespace Belerion.Games.ThreeInRowUnity.Spawning.Element
{
    public class ElementSpawningSystem : AbstractSpawningSystem<ElementSpawner, ElementCreationRequest>
    {
        protected override void Spawn(NativeArray<PrefabsBufferElement> prefabs, Entity sourceEntity, ref ElementCreationRequest creationRequest)
        {
            var newEntity = EntityManager.Instantiate(prefabs[(int)creationRequest.ElementType].Prefab);

            CopyComponents(EntityManager.GetComponentTypes(sourceEntity).Select(t => t.GetManagedType()), sourceEntity, newEntity);
        }
    }
}