﻿using System;
using System.Linq;
using Unity.Entities;
using UnityEngine;

namespace Belerion.Games.ThreeInRowUnity.Spawning.Element
{
    [DisallowMultipleComponent]
    [RequiresEntityConversion]
    public class ElementSpawnerAuthoring : AbstractSpawnerAuthoring<ElementTypes, ElementSpawner>
    {
        [Serializable]
        public class ElementPrefabKeyValue : AbstractPrefabInspectorKeyValue<ElementTypes> {}

        [SerializeField]
        private ElementPrefabKeyValue[] _prefabs;

        public void OnEnable()
        {
            PrefabsMap = _prefabs.ToDictionary(keyValue => keyValue.Key, keyValue => keyValue.Prefab);
        }
    }
}
