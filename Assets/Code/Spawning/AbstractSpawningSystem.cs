﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Unity.Collections;
using Unity.Entities;

namespace Belerion.Games.ThreeInRowUnity.Spawning
{
    public abstract class AbstractSpawningSystem<TSpawnerComponent, TCreationRequest> : ComponentSystem
        where TSpawnerComponent : struct, IComponentData
        where TCreationRequest : struct, IComponentData
    {
        private Entity _spawner;

        protected override void OnStartRunning()
        {
            _spawner = GetEntityQuery(ComponentType.ReadOnly<TSpawnerComponent>()).GetSingletonEntity();
        }

        protected override void OnUpdate()
        {
            var prefabs = GetBufferWithPrefabs().ToNativeArray(Allocator.Temp);

            Entities
                .WithAllReadOnly<TCreationRequest>()
                .ForEach((Entity entity, ref TCreationRequest creationRequest) =>
                {
                    Spawn(prefabs, entity, ref creationRequest);

                    EntityManager.DestroyEntity(entity);
                });
        }

        protected void CopyComponents(IEnumerable<Type> componentTypes, Entity sourceEntity, Entity destinationEntity)
        {
            foreach (var componentType in componentTypes.Where(t => t != typeof(TCreationRequest)))
            {
                GetType()
                    .GetMethods(BindingFlags.NonPublic | BindingFlags.Instance)
                    .Single(method => method.IsGenericMethod && method.Name == nameof(CopyComponent))
                    .MakeGenericMethod(componentType)
                    .Invoke(this, new object[] { sourceEntity, destinationEntity });
            }
        }

        protected void CopyComponent<TComponent>(Entity sourceEntity, Entity destinationEntity)
            where TComponent : struct, IComponentData
        {
            if (!EntityManager.HasComponent<TComponent>(destinationEntity))
            {
                EntityManager.AddComponent<TComponent>(destinationEntity);
            }

            if (ComponentType.ReadOnly<TComponent>().IsZeroSized)
            {
                return;
            }

            EntityManager.SetComponentData(destinationEntity, EntityManager.GetComponentData<TComponent>(sourceEntity));
        }

        protected abstract void Spawn(NativeArray<PrefabsBufferElement> prefabs, Entity sourceEntity, ref TCreationRequest creationRequest);

        private DynamicBuffer<PrefabsBufferElement> GetBufferWithPrefabs()
        {
            return GetBufferFromEntity<PrefabsBufferElement>(true)[_spawner];
        }
    }
}