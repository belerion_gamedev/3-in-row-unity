﻿using Unity.Entities;

namespace Belerion.Games.ThreeInRowUnity.UI
{
    public class UiEventsCleaningSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            EntityManager.DestroyEntity(
                GetEntityQuery(ComponentType.ReadOnly<UiEvent>()));
        }
    }
}