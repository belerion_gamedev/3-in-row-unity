﻿using System;
using System.Collections.Generic;
using Belerion.Games.ThreeInRowUnity.UI.EcsEvents;
using Unity.Entities;
using UnityEngine;

namespace Belerion.Games.ThreeInRowUnity.UI
{
    public static class EventHub
    {
        private static IDictionary<Type, IList<object>> _eventHandlersMap = new Dictionary<Type, IList<object>>();

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        private static void Init()
        {
            _eventHandlersMap = new Dictionary<Type, IList<object>>();
        }

        public static void RegisterHandler<TEcsEvent>(Action<TEcsEvent> eventHandler)
            where TEcsEvent : BaseEcsEvent
        {
            if (!_eventHandlersMap.TryGetValue(typeof(TEcsEvent), out var eventHandlersList))
            {
                eventHandlersList = new List<object>();
                _eventHandlersMap.Add(typeof(TEcsEvent), eventHandlersList);
            }

            eventHandlersList.Add(eventHandler);
        }

        public static void PublishEventToUi<TEcsEvent>(TEcsEvent ecsEvent)
            where TEcsEvent : BaseEcsEvent
        {
            if (_eventHandlersMap.TryGetValue(typeof(TEcsEvent), out var eventHandlersList))
            {
                foreach (var eventHandler in eventHandlersList)
                {
                    (eventHandler as Action<TEcsEvent>).Invoke(ecsEvent);
                }
            }
        }

        public static void PublishEventToEcs(UiEventTypes eventType)
        {
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var eventEntity = entityManager.CreateEntity();
            entityManager.AddComponentData(eventEntity, new UiEvent { Type = eventType });
        }
    }
}
