﻿using Belerion.Games.ThreeInRowUnity.UI.EcsEvents;
using UnityEngine;
using UnityEngine.UI;

namespace Belerion.Games.ThreeInRowUnity.UI.Authoring
{
    public class NotInGameUiAuthoring : MonoBehaviour
    {
        public void Start()
        {
            transform.Find("StartButton").GetComponent<Button>().onClick.AddListener(
                () => EventHub.PublishEventToEcs(UiEventTypes.StartGame));

            EventHub.RegisterHandler<GameModeChangedEcsEvent>(OnGameModeChanged);
        }

        private void OnGameModeChanged(GameModeChangedEcsEvent ecsEvent)
        {
            if (ecsEvent.OldMode == GameModes.NotInGame)
            {
                gameObject.SetActive(false);
            }

            if (ecsEvent.NewMode == GameModes.NotInGame)
            {
                gameObject.SetActive(true);
            }
        }
    }
}
