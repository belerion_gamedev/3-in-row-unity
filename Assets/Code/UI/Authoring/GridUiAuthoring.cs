﻿using Belerion.Games.ThreeInRowUnity.Authoring;
using Belerion.Games.ThreeInRowUnity.UI.EcsEvents;
using UnityEngine;
using UnityEngine.UI;

namespace Belerion.Games.ThreeInRowUnity.UI.Authoring
{
    public class GridUiAuthoring : MonoBehaviour
    {
        [SerializeField]
        private Image _gridCellPrefab;

        private GameAuthoring _game;
        private RectTransform _rectTransform;

        private Image[,] _gridCells;

        public void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
            _game = FindObjectOfType<GameAuthoring>();

            SetupGrid();

            EventHub.RegisterHandler<ElementSelectedEcsEvent>(OnElementSelected);
            EventHub.RegisterHandler<ElementDeselectedEcsEvent>(OnElementDeselected);
            EventHub.RegisterHandler<GameModeChangedEcsEvent>(OnGameModeChanged);
        }

        private void SetupGrid()
        {
            _rectTransform.sizeDelta = new Vector2(_game.FieldXSize, _game.FieldYSize);
            _gridCells = new Image[_game.FieldXSize, _game.FieldYSize];

            for (var x = 0; x < _game.FieldXSize; x++)
            {
                for (var y = 0; y < _game.FieldYSize; y++)
                {
                    _gridCells[x, y] = Instantiate(_gridCellPrefab, new Vector3(x, y), Quaternion.identity, _rectTransform);
                }
            }
        }

        private void OnElementSelected(ElementSelectedEcsEvent ecsEvent)
        {
            _gridCells[ecsEvent.Position.x, ecsEvent.Position.y].color = Color.red;
        }

        private void OnElementDeselected(ElementDeselectedEcsEvent ecsEvent)
        {
            _gridCells[ecsEvent.Position.x, ecsEvent.Position.y].color = _gridCellPrefab.color;
        }

        private void OnGameModeChanged(GameModeChangedEcsEvent ecsEvent)
        {
            if (ecsEvent.NewMode != GameModes.FieldFilling) return;

            foreach (var image in _gridCells)
            {
                image.color = _gridCellPrefab.color;
            }
        }
    }
}
