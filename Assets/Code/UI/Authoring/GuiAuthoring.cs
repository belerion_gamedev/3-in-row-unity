﻿using Belerion.Games.ThreeInRowUnity.UI.EcsEvents;
using UnityEngine;
using UnityEngine.UI;

namespace Belerion.Games.ThreeInRowUnity.UI.Authoring
{
    public class GuiAuthoring : MonoBehaviour
    {
        private Canvas _canvas;
        private Text _scoreText;

        public void Awake()
        {
            _canvas = GetComponent<Canvas>();
            _scoreText = _canvas.transform.Find("Header").Find("Score").GetComponent<Text>();

            EventHub.RegisterHandler<ScoreIncreasedEcsEvent>(OnScoreIncreased);
        }

        private void OnScoreIncreased(ScoreIncreasedEcsEvent ecsEvent)
        {
            _scoreText.text = $"{int.Parse(_scoreText.text) + ecsEvent.IncreaseValue}";
        }
    }
}
