﻿namespace Belerion.Games.ThreeInRowUnity.UI.EcsEvents
{
    public class ScoreIncreasedEcsEvent : BaseEcsEvent
    {
        public int IncreaseValue { get; set; }
    }
}
