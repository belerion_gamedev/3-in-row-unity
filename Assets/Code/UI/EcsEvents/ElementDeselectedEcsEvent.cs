﻿using Unity.Mathematics;

namespace Belerion.Games.ThreeInRowUnity.UI.EcsEvents
{
    public class ElementDeselectedEcsEvent : BaseEcsEvent
    {
        public int2 Position { get; set; }
    }
}
