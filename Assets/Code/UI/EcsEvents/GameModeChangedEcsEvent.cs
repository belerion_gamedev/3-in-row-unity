﻿namespace Belerion.Games.ThreeInRowUnity.UI.EcsEvents
{
    public class GameModeChangedEcsEvent : BaseEcsEvent
    {
        public GameModes OldMode { get; set; }

        public GameModes NewMode { get; set; }
    }
}
