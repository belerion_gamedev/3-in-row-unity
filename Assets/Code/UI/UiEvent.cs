﻿using Unity.Entities;

namespace Belerion.Games.ThreeInRowUnity.UI
{
    public struct UiEvent : IComponentData
    {
        public UiEventTypes Type { get; set; }
    }
}
