﻿using System;
using System.Collections.Generic;
using System.Linq;
using Belerion.Games.ThreeInRowUnity.Components;
using Belerion.Games.ThreeInRowUnity.Components.Tags;
using Belerion.Games.ThreeInRowUnity.Helpers;
using Belerion.Games.ThreeInRowUnity.Spawning.Element;
using Unity.Entities;
using Unity.Mathematics;

namespace Belerion.Games.ThreeInRowUnity.Systems
{
    [UpdateAfter(typeof(ElementMovementSystem))]
    public class FieldFillingSystem : AbstractGameSystem, IGameModeAffectedSystem
    {
        public GameModes[] GameModesInWhichActive => new[] { GameModes.FieldFilling };

        protected override void OnUpdateInner()
        {
            var elements = new ElementTypes?[GameConfig.FieldSize.x];
            Element?[] movedElements = null;

            if (Entities.WithAnyReadOnly<ElementToSpawnTag>().ToEntityQuery().CalculateEntityCount() > 0)
            {
                movedElements = new Element?[GameConfig.FieldSize.x];
                Entities.WithAllReadOnly<ElementToSpawnTag, Element>()
                    .ForEach((Entity entity, ref Element element) =>
                    {
                        if (element.GridPosition.y <= GameConfig.FieldSize.y - 1)
                        {
                            movedElements[element.GridPosition.x] = element;
                            EntityManager.RemoveComponent<ElementToSpawnTag>(entity);
                        }
                        else
                        {
                            elements[element.GridPosition.x] = element.ElementType;
                        }
                    });

                if (Entities.WithAnyReadOnly<ElementMovingTag>().ToEntityQuery().CalculateEntityCount() == 0)
                {
                    ChangeGameMode(GameModes.CheckDestroyCombinationsOnField);
                }
            }

            GenerateNewToSpawnForMoved(elements, movedElements);
        }

        private void GenerateNewToSpawnForMoved(ElementTypes?[] elements, Element?[] movedElements = null)
        {
            for (var x = 0; x < GameConfig.FieldSize.x; x++)
            {
                if (movedElements == null || movedElements[x].HasValue)
                {
                    var availableTypes = GetAvailableElementTypes(x, elements, movedElements?[x]);

                    elements[x] = availableTypes.Any() ? availableTypes.ChooseRandom() : ElementTypes.Capsule;

                    CreateElementToSpawn(
                        x,
                        movedElements?[x].Value.CountOfElementsBelow + 1 ?? 0,
                        elements[x] == movedElements?[x]?.ElementType ? movedElements[x].Value.ChainToDown + 1 : 1,
                        elements[x].Value);
                }
            }
        }

        private ICollection<ElementTypes> GetAvailableElementTypes(int x, ElementTypes?[] elements, Element? movedElement)
        {
            var availableTypes = Enum.GetValues(typeof(ElementTypes)).Cast<ElementTypes>().ToList();

            if (movedElement?.ChainToDown >= 2)
            {
                availableTypes.Remove(movedElement.Value.ElementType);
            }

            if (x > 0 && GameConfig.FieldSize.x - x > 1 && elements[x - 1] == elements[x + 1]
                || x > 1 && elements[x - 1] == elements[x - 2])
            {
                availableTypes.Remove(elements[x - 1].Value);
            }

            if (GameConfig.FieldSize.x - x > 2 && elements[x + 1] == elements[x + 2] && elements[x + 1].HasValue)
            {
                availableTypes.Remove(elements[x + 1].Value);
            }

            return availableTypes;
        }

        private void CreateElementToSpawn(int x, int countBelow, int chainToDown, ElementTypes type)
        {
            var entity = EntityManager.CreateEntity(ComponentType.ReadWrite<ElementToSpawnTag>());

            EntityManager.AddComponentData(entity, new ElementCreationRequest { ElementType = type });
            EntityManager.AddComponentData(entity, new Element
            {
                ElementType = type,
                GridPosition = new int2(x, GameConfig.FieldSize.y),
                CountOfElementsBelow = (byte)countBelow,
                ChainToDown = (byte)chainToDown,
            });
        }
    }
}