﻿using Belerion.Games.ThreeInRowUnity.Animation;
using Belerion.Games.ThreeInRowUnity.Components;
using Belerion.Games.ThreeInRowUnity.Components.Tags;
using Unity.Entities;
using Unity.Mathematics;

namespace Belerion.Games.ThreeInRowUnity.Systems
{
    public class ElementMovementSystem : AbstractGameSystem, IGameModeAffectedSystem
    {
        public GameModes[] GameModesInWhichActive => new[] { GameModes.FieldFilling };

        protected override void OnUpdateInner()
        {
            ExecuteFallingAnimationCallback();
            EnsureCorrectElementMovingStatus();
            AnimateFalling();
        }

        private void EnsureCorrectElementMovingStatus()
        {
            Entities
                .WithNone<ElementMovingTag>()
                .ForEach((Entity entity, ref Element element) =>
                {
                    if (element.CountOfElementsBelow < element.GridPosition.y)
                    {
                        EntityManager.AddComponent<ElementMovingTag>(entity);
                        EntityManager.AddComponent<DestructionSeedTag>(entity);
                    }
                });
            Entities
                .WithAny<ElementMovingTag>()
                .ForEach((Entity entity, ref Element element) =>
                {
                    if (element.CountOfElementsBelow >= element.GridPosition.y)
                    {
                        EntityManager.RemoveComponent<ElementMovingTag>(entity);
                    }
                });
        }

        private void ExecuteFallingAnimationCallback()
        {
            Entities
                .WithAllReadOnly<AnimationEndedTag, ElementMovingTag>()
                .ForEach((Entity entity, ref Element element) =>
                {
                    element.GridPosition = new int2(element.GridPosition.x, element.GridPosition.y - 1);
                });
        }

        private void AnimateFalling()
        {
            Entities
                .WithNone<Animated>()
                .WithAny<ElementMovingTag>()
                .ForEach((Entity entity, ref Element element) =>
                {
                    EntityManager.AddComponentData(entity,
                        new Animated
                        {
                            Type = AnimationType.Linear,
                            Source = new float3(element.GridPosition, 0),
                            Destination = new float3(element.GridPosition - new int2(0, 1), 0),
                            StartedAtTime = Time.ElapsedTime,
                            TimeAllocatedForAnimation = GameConfig.TimeToFall
                        });
                });
        }
    }
}