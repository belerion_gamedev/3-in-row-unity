﻿using System;
using System.Collections.Generic;
using System.Linq;
using Belerion.Games.ThreeInRowUnity.Animation;
using Belerion.Games.ThreeInRowUnity.Components;
using Belerion.Games.ThreeInRowUnity.Components.Tags;
using Unity.Entities;
using Unity.Mathematics;

namespace Belerion.Games.ThreeInRowUnity.Systems
{
    public class ElementSwappingSystem : AbstractGameSystem, IGameModeAffectedSystem
    {
        public GameModes[] GameModesInWhichActive => new[] { GameModes.ElementsSwapping };

        protected override void OnStartRunning()
        {
            base.OnStartRunning();
            if (Entities.WithAllReadOnly<ElementSelectedTag>().ToEntityQuery().CalculateEntityCount() == 0)
            {
                ChangeGameMode(GameModes.WaitingUserInputOnField);
            }
        }

        protected override void OnUpdateInner()
        {
            var selected = GetNotAnimatedSelectedForSwapping();

            if (selected.Any())
            {
                if (selected.Count != 2)
                {
                    ChangeGameMode(GameModes.WaitingUserInputOnField);
                    throw new Exception("Exactly two elements must be selected before going into Swapping mode");
                }
                
                SwapElements(selected[0], selected[1]);
            }

            if (Entities.WithAllReadOnly<ElementSelectedTag, AnimationEndedTag>().ToEntityQuery().CalculateEntityCount() > 0)
            {
                HandleSwappingAnimationFinished();
            }
        }

        private List<(Entity entity, Element gridPosition)> GetNotAnimatedSelectedForSwapping()
        {
            var selected = new List<(Entity entity, Element gridPosition)>(2);

            Entities
                .WithAllReadOnly<ElementSelectedTag, AlreadySwappedTag>()
                .WithNone<Animated, AnimationEndedTag>()
                .ForEach((Entity entity, ref Element element) => selected.Add((entity, element)));

            if (!selected.Any())
            {
                Entities
                    .WithAllReadOnly<ElementSelectedTag>()
                    .WithNone<Animated, AnimationEndedTag>()
                    .ForEach((Entity entity, ref Element element) => selected.Add((entity, element)));
            }

            return selected;
        }

        private void SwapElements((Entity entity, Element element) selected1, (Entity entity, Element element) selected2)
        {
            EntityManager.AddComponentData(selected1.entity,
                new Animated
                {
                    Type = AnimationType.Linear,
                    Source = new float3(selected1.element.GridPosition, 0),
                    Destination = new float3(selected2.element.GridPosition, 0),
                    StartedAtTime = Time.ElapsedTime,
                    TimeAllocatedForAnimation = GameConfig.TimeToSwap
                });
            EntityManager.AddComponentData(selected2.entity,
                new Animated
                {
                    Type = AnimationType.Linear,
                    Source = new float3(selected2.element.GridPosition, 0),
                    Destination = new float3(selected1.element.GridPosition, 0),
                    StartedAtTime = Time.ElapsedTime,
                    TimeAllocatedForAnimation = GameConfig.TimeToSwap
                });

            var tempPosition = selected1.element.GridPosition;
            selected1.element.GridPosition = selected2.element.GridPosition;
            selected2.element.GridPosition = tempPosition;

            EntityManager.SetComponentData(selected1.entity, selected1.element);
            EntityManager.SetComponentData(selected2.entity, selected2.element);
        }

        private void HandleSwappingAnimationFinished()
        {
            if (Entities.WithAllReadOnly<AlreadySwappedTag>().ToEntityQuery().CalculateEntityCount() > 0)
            {
                Entities.WithAny<AlreadySwappedTag>()
                    .ForEach(entity => EntityManager.RemoveComponent<AlreadySwappedTag>(entity));
                ChangeGameMode(GameModes.WaitingUserInputOnField);
            }
            else
            {
                Entities.WithAny<ElementSelectedTag>()
                    .ForEach(entity =>
                    {
                        EntityManager.AddComponent<DestructionSeedTag>(entity);
                        EntityManager.AddComponent<AlreadySwappedTag>(entity);
                    });
                ChangeGameMode(GameModes.CheckDestroyCombinationsOnField);
            }
        }
    }
}