﻿using Belerion.Games.ThreeInRowUnity.UI;
using Unity.Entities;

namespace Belerion.Games.ThreeInRowUnity.Systems
{
    public class InputHandlingWhileNotInGameSystem : AbstractGameSystem, IGameModeAffectedSystem
    {
        public GameModes[] GameModesInWhichActive => new[] { GameModes.NotInGame };

        protected override void OnUpdateInner()
        {
            Entities.ForEach((Entity entity, ref UiEvent uiEvent) =>
            {
                switch (uiEvent.Type)
                {
                    case UiEventTypes.StartGame:
                        ChangeGameMode(GameModes.FieldFilling);
                        break;
                }
            });
        }
    }
}