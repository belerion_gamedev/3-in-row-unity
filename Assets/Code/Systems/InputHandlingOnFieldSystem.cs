﻿using Belerion.Games.ThreeInRowUnity.Components;
using Belerion.Games.ThreeInRowUnity.Components.Tags;
using Belerion.Games.ThreeInRowUnity.UI;
using Belerion.Games.ThreeInRowUnity.UI.EcsEvents;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Belerion.Games.ThreeInRowUnity.Systems
{
    public class InputHandlingOnFieldSystem : AbstractGameSystem, IGameModeAffectedSystem
    {
        public GameModes[] GameModesInWhichActive => new [] { GameModes.WaitingUserInputOnField };

        protected override void OnStartRunning()
        {
            base.OnStartRunning();

            Entities.WithAny<ElementSelectedTag>()
                .ForEach((Entity entity, ref Element element) => DeselectElement(entity, element.GridPosition));
        }

        protected override void OnUpdateInner()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var inputPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                var gridPoint = new int2((int) (inputPosition.x + 0.5), (int) (inputPosition.y + 0.5));
                if (math.any(gridPoint < new int2(0, 0)) || math.any(gridPoint >= GameConfig.FieldSize))
                {
                    return;
                }

                HandleClickOnGridCell(gridPoint);
            }
        }

        private void HandleClickOnGridCell(int2 gridPoint)
        {
            var selectedElementQuery = Entities.WithAny<ElementSelectedTag>().ToEntityQuery();
            if (selectedElementQuery.CalculateEntityCount() != 0)
            {
                var selectedEntity = selectedElementQuery.GetSingletonEntity();
                var selectedElement = EntityManager.GetComponentData<Element>(selectedEntity);
                var distanceToSelected = math.distance(gridPoint, selectedElement.GridPosition);

                if (distanceToSelected >= 2 || distanceToSelected == 0)
                {
                    DeselectElement(selectedEntity, selectedElement.GridPosition);
                    return;
                }

                ChangeGameMode(GameModes.ElementsSwapping);
            }

            Entities.WithNone<ElementSelectedTag>()
                .ForEach((Entity entity, ref Element element) =>
                {
                    if (!math.all(element.GridPosition == gridPoint)) return;

                    SelectElement(entity, gridPoint);
                });
        }

        private void SelectElement(Entity entity, int2 gridPoint)
        {
            EntityManager.AddComponent<ElementSelectedTag>(entity);
            EventHub.PublishEventToUi(new ElementSelectedEcsEvent { Position = gridPoint });
        }

        private void DeselectElement(Entity entity, int2 gridPoint)
        {
            EntityManager.RemoveComponent<ElementSelectedTag>(entity);
            EventHub.PublishEventToUi(new ElementDeselectedEcsEvent { Position = gridPoint });
        }
    }
}