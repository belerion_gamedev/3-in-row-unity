﻿using Belerion.Games.ThreeInRowUnity.Components;
using Belerion.Games.ThreeInRowUnity.UI;
using Belerion.Games.ThreeInRowUnity.UI.EcsEvents;
using Unity.Entities;

namespace Belerion.Games.ThreeInRowUnity.Systems
{
    public abstract class AbstractGameSystem : ComponentSystem
    {
        protected Entity GameEntity;
        protected GameState GameState;
        protected GameConfig GameConfig;

        protected void ChangeGameMode(GameModes newGameMode)
        {
            EventHub.PublishEventToUi(
                new GameModeChangedEcsEvent { NewMode = newGameMode, OldMode = GameState.CurrentGameMode });
            GameState.CurrentGameMode = newGameMode;
            EntityManager.SetComponentData(GameEntity, GameState);
        }

        protected override void OnUpdate()
        {
            GameEntity = GetEntityQuery(ComponentType.ReadWrite<GameState>(), ComponentType.ReadOnly<GameConfig>()).GetSingletonEntity();
            GameState = GetComponentDataFromEntity<GameState>(true)[GameEntity];
            GameConfig = GetComponentDataFromEntity<GameConfig>(true)[GameEntity];

            this.OnUpdateInner();
        }

        protected abstract void OnUpdateInner();
    }
}