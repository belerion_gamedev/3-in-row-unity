﻿using System.Collections.Generic;
using System.Linq;
using Belerion.Games.ThreeInRowUnity.Components;
using Belerion.Games.ThreeInRowUnity.Components.Tags;
using Belerion.Games.ThreeInRowUnity.UI;
using Belerion.Games.ThreeInRowUnity.UI.EcsEvents;
using Unity.Entities;
using Unity.Mathematics;

namespace Belerion.Games.ThreeInRowUnity.Systems
{
    public class ElementDestructionSystem : AbstractGameSystem, IGameModeAffectedSystem
    {
        public GameModes[] GameModesInWhichActive => new[] { GameModes.CheckDestroyCombinationsOnField };

        protected override void OnUpdateInner()
        {
            MarkForDestructionAroundSeeds();

            var isSomethingRemoved = DestroyMarkedElements();

            if (isSomethingRemoved)
            {
                Entities.WithAllReadOnly<ElementSelectedTag>()
                    .ForEach(entity => EntityManager.RemoveComponent<ElementSelectedTag>(entity));

                ChangeGameMode(GameModes.FieldFilling);
            }
            else
            {
                var newGameMode = Entities.WithAllReadOnly<AlreadySwappedTag>().ToEntityQuery().CalculateEntityCount() > 0
                    ? GameModes.ElementsSwapping
                    : GameModes.WaitingUserInputOnField;
                ChangeGameMode(newGameMode);
            }
        }

        private bool DestroyMarkedElements()
        {
            var removedInColumns = Enumerable.Repeat(0, GameConfig.FieldSize.x).Select(i => new HashSet<int>()).ToArray();
            Entities.WithAllReadOnly<MarkedForDestroyTag, Element>()
                .ForEach((Entity entity, ref Element element) =>
                {
                    removedInColumns[element.GridPosition.x].Add(element.GridPosition.y);
                    EntityManager.DestroyEntity(entity);
                    EventHub.PublishEventToUi(new ScoreIncreasedEcsEvent {IncreaseValue = 1});
                });

            Entities
                .ForEach((Entity entity, ref Element element) =>
                {
                    var removedInColumn = removedInColumns[element.GridPosition.x];
                    foreach (var removed in removedInColumn)
                    {
                        element.CountOfElementsBelow -= (byte) (element.GridPosition.y > removed ? 1 : 0);
                    }
                });

            return removedInColumns.Any(removedInColumn => removedInColumn.Any());
        }

        private void MarkForDestructionAroundSeeds()
        {
            var entities = new Entity[GameConfig.FieldSize.x, GameConfig.FieldSize.y];
            var elements = new Element[GameConfig.FieldSize.x, GameConfig.FieldSize.y];

            Entities.WithNone<ElementToSpawnTag>()
                .ForEach((Entity entity, ref Element element) =>
                {
                    entities[element.GridPosition.x, element.GridPosition.y] = entity;
                    elements[element.GridPosition.x, element.GridPosition.y] = element;
                });

            Entities.WithAllReadOnly<DestructionSeedTag, Element>()
                .ForEach((Entity entity, ref Element element) =>
                {
                    var position = element.GridPosition;
                    var elementType = element.ElementType;

                    var leftMatch = MarkLeftMatch(ref elements, ref entities, ref entity, ref position, ref elementType);
                    var rightMatch = MarkRightMatch(ref elements, ref entities, ref entity, ref position, ref elementType);
                    var downMatch = MarkDownMatch(ref elements, ref entities, ref entity, ref position, ref elementType);
                    var upMatch = MarkUpMatch(ref elements, ref entities, ref entity, ref position, ref elementType);
                    var centerHorizontalMatch = MarkCenterHorizontalMatch(ref elements, ref entities, ref entity, ref position, ref elementType);
                    var centerVerticalMatch = MarkCenterVerticalMatch(ref elements, ref entities, ref entity, ref position, ref elementType);

                    //TODO: Code to convert current seed into some power-up depending on matched

                    EntityManager.RemoveComponent<DestructionSeedTag>(entity);
                });
        }

        private bool MarkLeftMatch(ref Element[,] elements, ref Entity[,] entities, ref Entity entity, ref int2 position, ref ElementTypes elementType)
        {
            return position.x - 2 >= 0 
                && MarkMatch(ref elements, ref entities, ref entity, ref position, ref elementType, position - new int2(1, 0), position - new int2(2, 0));
        }

        private bool MarkRightMatch(ref Element[,] elements, ref Entity[,] entities, ref Entity entity, ref int2 position, ref ElementTypes elementType)
        {
            return position.x + 2 < GameConfig.FieldSize.x
                && MarkMatch(ref elements, ref entities, ref entity, ref position, ref elementType, position + new int2(1, 0), position + new int2(2, 0));
        }

        private bool MarkDownMatch(ref Element[,] elements, ref Entity[,] entities, ref Entity entity, ref int2 position, ref ElementTypes elementType)
        {
            return position.y - 2 >= 0
                && MarkMatch(ref elements, ref entities, ref entity, ref position, ref elementType, position - new int2(0, 1), position - new int2(0, 2));
        }

        private bool MarkUpMatch(ref Element[,] elements, ref Entity[,] entities, ref Entity entity, ref int2 position, ref ElementTypes elementType)
        {
            return position.y + 2 < GameConfig.FieldSize.y
                && MarkMatch(ref elements, ref entities, ref entity, ref position, ref elementType, position + new int2(0, 1), position + new int2(0, 2));
        }

        private bool MarkCenterHorizontalMatch(ref Element[,] elements, ref Entity[,] entities, ref Entity entity, ref int2 position, ref ElementTypes elementType)
        {
            return position.x + 1 < GameConfig.FieldSize.x && position.x - 1 >= 0
                && MarkMatch(ref elements, ref entities, ref entity, ref position, ref elementType, position - new int2(1, 0), position + new int2(1, 0));
        }

        private bool MarkCenterVerticalMatch(ref Element[,] elements, ref Entity[,] entities, ref Entity entity, ref int2 position, ref ElementTypes elementType)
        {
            return position.y + 1 < GameConfig.FieldSize.y && position.y - 1 >= 0
                && MarkMatch(ref elements, ref entities, ref entity, ref position, ref elementType, position - new int2(0, 1), position + new int2(0, 1));
        }

        private bool MarkMatch(ref Element[,] elements, ref Entity[,] entities, ref Entity entity, ref int2 position, ref ElementTypes elementType, int2 position2, int2 position3)
        {
            var match = elementType == elements[position2.x, position2.y].ElementType
                        && elementType == elements[position3.x, position3.y].ElementType;

            if (match)
            {
                EntityManager.AddComponent<MarkedForDestroyTag>(entity);
                EntityManager.AddComponent<MarkedForDestroyTag>(entities[position2.x, position2.y]);
                EntityManager.AddComponent<MarkedForDestroyTag>(entities[position3.x, position3.y]);
            }

            return match;
        }
    }
}