﻿namespace Belerion.Games.ThreeInRowUnity.Systems
{
    public interface IGameModeAffectedSystem
    {
        GameModes[] GameModesInWhichActive { get; }

        bool Enabled { get; set; }
    }
}