﻿using Unity.Entities;
using Unity.Mathematics;

namespace Belerion.Games.ThreeInRowUnity.Components
{
    public struct GameConfig : IComponentData
    {
        public int2 FieldSize;

        public double TimeToSwap;
        public double TimeToFall;
    }
}
