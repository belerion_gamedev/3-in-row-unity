﻿using Unity.Entities;

namespace Belerion.Games.ThreeInRowUnity.Components.Tags
{
    public struct MarkedForDestroyTag : IComponentData
    {
    }
}