﻿using Unity.Entities;
using Unity.Mathematics;

namespace Belerion.Games.ThreeInRowUnity.Components
{
    public struct Element : IComponentData
    {
        public ElementTypes ElementType;

        public int2 GridPosition;

        public byte CountOfElementsBelow;
        
        public byte ChainToDown;
    }
}
