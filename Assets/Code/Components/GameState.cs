﻿using Unity.Entities;

namespace Belerion.Games.ThreeInRowUnity.Components
{
    public struct GameState : IComponentData
    {
        public GameModes CurrentGameMode;
    }
}
