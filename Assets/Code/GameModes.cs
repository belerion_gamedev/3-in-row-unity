﻿namespace Belerion.Games.ThreeInRowUnity
{
    public enum GameModes
    {
        NotInGame,
        FieldFilling,
        WaitingUserInputOnField,
        ElementsSwapping,
        CheckDestroyCombinationsOnField,
    }
}
