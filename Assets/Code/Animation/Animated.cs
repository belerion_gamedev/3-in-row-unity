﻿using Unity.Entities;
using Unity.Mathematics;

namespace Belerion.Games.ThreeInRowUnity.Animation
{
    public struct Animated : IComponentData
    {
        public AnimationType Type;
        public float3 Source;
        public float3 Destination;

        public double StartedAtTime;
        public double TimeAllocatedForAnimation;
    }
}