﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Belerion.Games.ThreeInRowUnity.Animation
{
    public class AnimationSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            var elapsedTime = Time.ElapsedTime;
            Entities
                .WithAny<AnimationEndedTag>()
                .ForEach(entity =>
                {
                    EntityManager.RemoveComponent<AnimationEndedTag>(entity);
                });

            Entities
                .ForEach((Entity entity, ref Translation translation, ref Animated animated) =>
                {
                    var t = (float)((elapsedTime - animated.StartedAtTime) / animated.TimeAllocatedForAnimation);
                    var tClamped = math.clamp(t, 0, 1);

                    switch (animated.Type)
                    {
                        case AnimationType.Linear:
                            translation.Value = math.lerp(animated.Source, animated.Destination, tClamped);
                            break;
                        case AnimationType.Smooth:
                            translation.Value = animated.Source + (animated.Destination - animated.Source) * math.smoothstep(0, 1, tClamped);
                            break;
                        case AnimationType.Stepped:
                            break;
                    }

                    if (t >= 1)
                    {
                        translation.Value = animated.Destination;
                        EntityManager.RemoveComponent<Animated>(entity);
                        EntityManager.AddComponent<AnimationEndedTag>(entity);
                    }
                });
        }
    }
}
