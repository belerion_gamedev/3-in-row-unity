﻿namespace Belerion.Games.ThreeInRowUnity.Animation
{
    public enum AnimationType
    {
        Stepped,
        Linear,
        Smooth
    }
}