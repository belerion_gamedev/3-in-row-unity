﻿namespace Belerion.Games.ThreeInRowUnity
{
    public enum ElementTypes
    {
        Capsule = 0,
        Cube = 1,
        Sphere = 2,
        Sphere2 = 3,
        Sphere3 = 4
    }
}