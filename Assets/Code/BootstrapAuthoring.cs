﻿using System.Linq;
using Belerion.Games.ThreeInRowUnity.Animation;
using Belerion.Games.ThreeInRowUnity.Helpers;
using Belerion.Games.ThreeInRowUnity.Spawning.Element;
using Belerion.Games.ThreeInRowUnity.Systems;
using Belerion.Games.ThreeInRowUnity.UI;
using Belerion.Games.ThreeInRowUnity.UI.EcsEvents;
using Unity.Entities;
using UnityEngine;

[assembly: DisableAutoCreation]
namespace Belerion.Games.ThreeInRowUnity
{
    public class BootstrapAuthoring : MonoBehaviour
    {
        private World _currentWorld;
        private IGameModeAffectedSystem[] _gameModeAffectedSystems;

        public void Start()
        {
            _currentWorld = World.DefaultGameObjectInjectionWorld;

            _currentWorld.GetOrCreateSystem<InitializationSystemGroup>()
                .AddNewOrExistingSystem<ElementSpawningSystem>();

            _currentWorld.GetOrCreateSystem<SimulationSystemGroup>()
                .AddNewOrExistingSystem<AnimationSystem>()
                .AddNewOrExistingSystem<ElementSwappingSystem>()
                .AddNewOrExistingSystem<InputHandlingOnFieldSystem>()
                .AddNewOrExistingSystem<InputHandlingWhileNotInGameSystem>()
                .AddNewOrExistingSystem<ElementMovementSystem>()
                .AddNewOrExistingSystem<ElementDestructionSystem>()
                .AddNewOrExistingSystem<FieldFillingSystem>();

            _currentWorld.GetOrCreateSystem<PresentationSystemGroup>()
                .AddNewOrExistingSystem<UiEventsCleaningSystem>();

            _gameModeAffectedSystems = _currentWorld.Systems
                .Select(system => system as IGameModeAffectedSystem)
                .Where(system => system != null)
                .ToArray();

            EventHub.RegisterHandler<GameModeChangedEcsEvent>(OnGameModeChanged);

            EventHub.PublishEventToUi(new GameModeChangedEcsEvent { NewMode = GameModes.NotInGame });
        }

        private void OnGameModeChanged(GameModeChangedEcsEvent ecsEvent)
        {
            foreach (var system in _gameModeAffectedSystems)
            {
                system.Enabled = system.GameModesInWhichActive.Contains(ecsEvent.NewMode);
            }
        }
    }
}
