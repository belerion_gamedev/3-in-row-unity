﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Belerion.Games.ThreeInRowUnity.Helpers
{
    public static class RandomExtensions
    {
        public static TItem ChooseRandom<TItem>(this ICollection<TItem> items)
        {
            return items.Skip(Random.Range(0, items.Count)).First();
        }
    }
}
