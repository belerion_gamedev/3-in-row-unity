﻿using Unity.Entities;

namespace Belerion.Games.ThreeInRowUnity.Helpers
{
    public static class EcsExtensions
    {
        public static ComponentSystemGroup AddNewOrExistingSystem<TSystem>(this ComponentSystemGroup group)
            where TSystem : ComponentSystemBase
        {
            var system = group.World.GetOrCreateSystem<TSystem>();
            group.AddSystemToUpdateList(system);

            return group;
        }
    }
}
