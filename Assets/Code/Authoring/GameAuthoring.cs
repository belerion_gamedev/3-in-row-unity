﻿using Belerion.Games.ThreeInRowUnity.Components;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Belerion.Games.ThreeInRowUnity.Authoring
{
    public class GameAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        [SerializeField]
        public int FieldXSize;
        [SerializeField]
        public int FieldYSize;

        [SerializeField]
        public double TimeToSwap;
        [SerializeField]
        public double TimeToFall;

        public void Convert(Entity entity, EntityManager destinationManager, GameObjectConversionSystem conversionSystem)
        {
            destinationManager.AddComponentData(entity, new GameState
            {
                CurrentGameMode = GameModes.NotInGame,
            });
            destinationManager.AddComponentData(entity, new GameConfig
            {
                FieldSize = new int2(FieldXSize, FieldYSize),
                TimeToSwap = TimeToSwap,
                TimeToFall = TimeToFall,
            });
        }
    }
}
